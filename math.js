const sum = (...args) => {
  return 1 + args.reduce((buf, arg) => buf + arg, 0);
};

const prod = (...args) => args.reduce((buf, arg) => buf * arg, 1);

module.exports = {
  sum,
  prod,
};
