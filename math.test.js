const { sum, prod } = require('./math');

test('adds 1 + 2 + 3 to equal 6', () => {
  expect(sum(1, 2, 3)).toBe(6);
});

test('2 * 2 expected to be 4', () => {
  expect(prod(2, 2)).toBe(4);
});
